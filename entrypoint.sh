#!/bin/sh

# A beginning user should be able to docker run image bash (or sh) without
# needing to learn about --entrypoint
# https://github.com/docker-library/official-images#consistency

[ -n "${PUID}" -a "${PUID}" != "$(id -u git)" ] && usermod -u ${PUID} git
[ -n "${PGID}" -a "${PGID}" != "$(id -g git)" ] && groupmod -g ${PGID} git

# prepare arg quoting
for x in "${@}"
do
  # try to figure out if quoting was required for the $x
  if [ "$x" != "${x%[[:space:]]*}" ]
  then
    x="\""$x"\""
  fi
  _args=$_args" "$x
done

set -e

# run command if it is not starting with a "-", is not a git subcommand and is an executable in PATH
if [ "$#" -gt 0 -a "${1#-}" == "$1" -a ! -x "/usr/libexec/git-core/git-$1" ]
then
  cmd="$_args"
else
  cmd="git $_args"
fi

# run as root if PUID or PGID is not set
if [ -n "${PUID}" -o -n "${PGID}" ]
then
  if grep -q "^$1$" /etc/shells
  then
    su -s "$@" git
  else
    su -s /bin/sh -c "$cmd" git
  fi
else
  exec $cmd
fi
