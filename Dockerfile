FROM alpine

ENV PUID=1000
ENV PGID=1000

COPY entrypoint.sh /entrypoint.sh

RUN apk --update --no-cache add \
      shadow git git-perl openssh-client ca-certificates && \
    echo "root:x:0:0:root:/root:/bin/sh" >| /etc/passwd && \
    echo "root:x:0:root" >| /etc/group && \
    echo "root:::0:::::" >| /etc/shadow && \
    addgroup -g "${PGID}" git && \
    adduser -u "${PUID}" -G git -s /sbin/nologin -g "Git User" -D git && \
    chmod +x /entrypoint.sh

VOLUME /home/git
VOLUME /git
WORKDIR /git

ENTRYPOINT ["/entrypoint.sh"]
CMD ["--help"]
